#!/bin/bash
# This script will quickly setup a session with a predefined layout on one window.
SESSION="WORK"

tmux kill-session -t $SESSION
tmux new-session -d -s $SESSION
tmux split-window -h -l 80 -t $SESSION:1.0
tmux split-window -v -t $SESSION:1.0
tmux split-window -h -t $SESSION:1.2
#tmux send-keys -t $SESSION:1.1 "cd $WORK_DIR && vim notes" Enter
#tmux send-keys -t $SESSION:1.0 "resize-pane -L 40" Enter
tmux send-keys -t $SESSION:1.0 "minimal; tree . " Enter
tmux send-keys -t $SESSION:1.1 "btm" Enter
tmux attach-session -t $SESSION
