#!/bin/bash
#xmodmap -e "keycode 98 = equal"
#xmodmap -e "keycode 99 = equal"
#xmodmap -e "keycode 100 = underscore"
#xmodmap -e "keycode 101 = equal"
#xmodmap -e "keycode 102 = minus"
#xmodmap -e "keycode 98 = space"
#xmodmap -e "keycode 99 = space"
#xmodmap -e "keycode 100 = space"
#xmodmap -e "keycode 101 = space"
#xmodmap -e "keycode 102 = space"

# fn key to ctrl
xmodmap -e "keycode 151 = Control_L NoSymbol Control_L"
# printscreen to tilde
xmodmap -e "keycode 107 = Alt_R Meta_R Alt_R Meta_R"
xmodmap -e "keycode 108 = grave asciitilde"
