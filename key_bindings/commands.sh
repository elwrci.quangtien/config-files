## Dumping
dconf dump /org/mate/marco/window-keybindings/ > dconf-mate-marco-keybindings.conf
dconf dump /org/mate/desktop/keybindings/ > dconf-mate-desktop-keybindings.conf

## Loading
cat dconf-mate-desktop-keybindings.conf | dconf load /org/mate/desktop/keybindings/
cat dconf-mate-marco-keybindings.conf | dconf load /org/mate/marco/window-keybindings/
