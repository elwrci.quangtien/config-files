set nocompatible              " be iMproved, required
filetype off                  " required

syntax on
set hlsearch
set hidden
set laststatus=2
set ignorecase
set incsearch
set number
set relativenumber
set autoindent
set noswapfile
set nobackup
let mapleader = "'"
set scrolloff=8
set background=dark

set ts=4 sw=4 expandtab
set smartindent
syntax on
if has('termguicolors')
  set termguicolors
  set t_ut=
endif
colorscheme hackthebox
hi StatusLine ctermbg=white ctermfg=black
